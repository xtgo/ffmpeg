package ffilter

import (
	"regexp"
	"strconv"
)

// ToFFMPEG will transform mpv-style 'vid1' and 'aid1' names
// into '0:v' and '0:a'; 'vo' and 'ao' outputs will be removed.
//
// This allows filter-generating algorithms to be written
// using an mpv input/output style, while still being able to target
// ffmpeg itself.
//
func ToFFMPEG(f Filter) Filter {
	return *toFFMPEG(&f)
}

func toFFMPEG(f *Filter) *Filter {
	if f == nil {
		return nil
	}

	inputs, inChanged := replaceAIDVID(f.Inputs)
	outputs, outChanged := removeAOVO(f.Outputs)
	prev := toFFMPEG(f.Prev)

	if inChanged || outChanged || prev != f.Prev {
		g := *f
		g.Inputs = inputs
		g.Outputs = outputs
		g.Prev = prev
		f = &g
	}

	return f
}

func removeAOVO(ss []string) (result []string, changed bool) {
	var numRemovals int

	for _, s := range ss {
		if s == "ao" || s == "vo" {
			numRemovals++
		}
	}

	if numRemovals == 0 {
		return ss, false
	}

	result = make([]string, 0, len(ss)-numRemovals)

	for _, s := range ss {
		if s != "ao" && s != "vo" {
			result = append(result, s)
		}
	}

	return result, true
}

func replaceAIDVID(ss []string) (result []string, changed bool) {
	buf := make([]byte, 0, 4)

	for i, s := range ss {
		if !mpvInputPattern.MatchString(s) {
			continue
		}

		if result == nil {
			result = append([]string(nil), ss...)
		}

		typ, n := parseMPVInput(s)
		n-- // mpv is 1-indexed, while ffmpeg is 0-indexed

		buf = strconv.AppendInt(buf[:0], int64(n), 10)
		buf = append(buf, ':', typ)
		result[i] = string(buf)
	}

	if result != nil {
		return result, true
	}

	return ss, false
}

//nolint: gochecknoglobals
var mpvInputPattern = regexp.MustCompile(`^[av]id\d+$`)

func parseMPVInput(s string) (byte, int) {
	i := len("vid")
	n, _ := strconv.Atoi(s[i:])

	return s[0], n
}
