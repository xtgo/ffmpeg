package ffilter

import (
	"bytes"
	"strconv"
)

// writer has two modes: byte counting, to reduce allocations
// (assuming that scanning elements is cheaper than repeated allocations),
// and a write buffering mode (when the buf field is non-nil).
//
type writer struct {
	size int
	buf  *bytes.Buffer
}

func (w *writer) writeString(s string) {
	if w.buf == nil {
		w.size += len(s)
		return
	}

	w.buf.WriteString(s)
}

func (w *writer) setBuffer(buf []byte) {
	w.buf = bytes.NewBuffer(buf)
	w.buf.Grow(w.size)
}

type separators struct{ graph, chain, token, arg0, argn, keyval string }

func (s *separators) arg(i int) string {
	if i == 0 {
		return s.arg0
	}

	return s.argn
}

func (s *separators) forIsChain(isChain bool) string {
	if isChain {
		return s.chain
	}

	return s.graph
}

func getSep(pretty bool) separators {
	if !pretty {
		return separators{
			graph:  ";",
			chain:  ",",
			token:  "",
			arg0:   "=",
			argn:   ":",
			keyval: "=",
		}
	}

	return separators{
		graph:  ";\n",
		chain:  ", ",
		token:  " ",
		arg0:   "=",
		argn:   ":",
		keyval: "=",
	}
}

func format(dst []byte, f *Filter, sep *separators) []byte {
	var w writer

	// count number of bytes we'll write
	formatFilter(&w, f, sep)

	if w.size == 0 {
		// return no-op default for completely empty filter chains
		return append(dst, "copy;acopy"...)
	}

	w.setBuffer(dst)

	// now actually write (append) bytes
	formatFilter(&w, f, sep)

	return w.buf.Bytes()
}

func formatFilter(w *writer, f *Filter, sep *separators) {
	if f == nil {
		return
	}

	// recursively format previous filter first
	formatFilter(w, f.Prev, sep)

	if f.Prev != nil && !f.IsEmpty() {
		// only write a separator if this filter is non-empty
		w.writeString(sep.forIsChain(f.IsChain))
	} else if f.IsEmpty() {
		// if this filter is empty, skip processing it
		return
	}

	if len(f.Inputs) != 0 {
		formatLabels(w, f.Inputs)
		w.writeString(sep.token)
	}

	w.writeString(f.Name)
	formatArgs(w, f.Args, sep)
	formatNamedArgs(w, f.NamedArgs, sep, len(f.Args))

	if len(f.Outputs) != 0 {
		w.writeString(sep.token)
		formatLabels(w, f.Outputs)
	}
}

func formatLabels(w *writer, labels []string) {
	for _, label := range labels {
		w.writeString("[")
		w.writeString(label)
		w.writeString("]")
	}
}

func formatArgs(w *writer, args []string, sep *separators) {
	for i, arg := range args {
		w.writeString(sep.arg(i))
		w.writeString(arg)
	}
}

func formatNamedArgs(w *writer, args []NamedArg, sep *separators, argnum int) {
	for i, arg := range args {
		w.writeString(sep.arg(argnum + i))
		w.writeString(arg.Name)
		w.writeString(sep.keyval)
		w.writeString(arg.Value)
	}
}

func formatSize(dst []byte, w, h uint16) []byte {
	if dst == nil {
		dst = make([]byte, 0, len("65535x65535"))
	}

	dst = strconv.AppendUint(dst, uint64(w), 10)
	dst = append(dst, 'x')
	dst = strconv.AppendUint(dst, uint64(h), 10)

	return dst
}
