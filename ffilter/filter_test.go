package ffilter_test

import (
	"fmt"
	"testing"

	"gitlab.com/xtgo/ffmpeg/ffilter"
)

func Example() {
	// example from https://ffmpeg.org/ffmpeg-filters.html#Filtering-Introduction
	filter := ffilter.New("split").Out("main").Out("tmp").
		New("crop").In("tmp").Arg("iw").Arg("ih/2").ArgU16(0).ArgU16(0).
		Chain("vflip").Out("flip").
		New("overlay").In("main").In("flip").ArgU16(0).Arg("H/2")

	fmt.Println(filter)

	// Output:
	// split[main][tmp];[tmp]crop=iw:ih/2:0:0,vflip[flip];[main][flip]overlay=0:H/2
}

func ExampleFilter_Format() {
	// example from https://ffmpeg.org/ffmpeg-filters.html#Filtering-Introduction
	filter := ffilter.New("split").Out("main").Out("tmp").
		New("crop").In("tmp").Arg("iw").Arg("ih/2").ArgU16(0).ArgU16(0).
		Chain("vflip").Out("flip").
		New("overlay").In("main").In("flip").ArgU16(0).Arg("H/2")

	fmt.Println(filter.Format())

	// Output:
	// split [main][tmp];
	// [tmp] crop=iw:ih/2:0:0, vflip [flip];
	// [main][flip] overlay=0:H/2
}

func Example_named_args() {
	const (
		w, h     = 1280, 720
		multiple = 2
	)

	// target 1280x720 display, but scale down further to retain aspect ratio;
	// example from https://ffmpeg.org/ffmpeg-filters.html#scale-1
	filter := ffilter.New("scale").
		Set("force_original_aspect_ratio", "decrease").
		SetU16("force_divisible_by", multiple).
		ArgU16(w).ArgU16(h)

	fmt.Println(filter)

	// Output:
	// scale=1280:720:force_original_aspect_ratio=decrease:force_divisible_by=2
}

func Example_typed_methods() {
	const (
		w, h  = 640, 512
		alpha = 0.5
		angle = 135.0
	)

	// show blurred audio spectrum render overlaid onto a mandelbrot render.
	filter := ffilter.New("mandelbrot").SetSize("s", w, h).Out("m").
		New("showspectrum").ArgSize(w, h).
		Chain("dblur").ArgFloat(angle).
		Chain("format").Arg("yuva444p").
		Chain("colorchannelmixer").SetFloat("aa", alpha).Out("w").
		New("overlay").In("m").In("w").
		Chain("format").Arg("yuv420p")

	fmt.Println(filter.Format())

	// Output:
	// mandelbrot=s=640x512 [m];
	// showspectrum=640x512, dblur=135, format=yuva444p, colorchannelmixer=aa=0.5 [w];
	// [m][w] overlay, format=yuv420p
}

func TestFilter_String_empty(t *testing.T) {
	got := ffilter.Filter{}.String()
	want := "copy;acopy"

	if got != want {
		t.Fatalf("got: %q, want: %q", got, want)
	}
}

func TestFilter_diverge(t *testing.T) {
	const numLabels = 5

	// contrived example to demonstrate that [non-concurrent]
	// slice-overwrites do not occur with divergent filters.
	base := ffilter.New("split").ArgInt(numLabels).
		Out("p").Out("q").Out("r").Out("s").Out("t").
		New("hstack").SetInt("inputs", numLabels).
		SetInt("shortest", 1).SetInt("repeatlast", 0).
		In("p").In("q").In("r")

	got1 := base.In("s").In("t").Set("eof_action", "endall").String()
	got2 := base.In("t").In("s").Set("eof_action", "pass").String()

	want1 := "split=5[p][q][r][s][t];[p][q][r][s][t]hstack=inputs=5:shortest=1:repeatlast=0:eof_action=endall"
	want2 := "split=5[p][q][r][s][t];[p][q][r][t][s]hstack=inputs=5:shortest=1:repeatlast=0:eof_action=pass"

	if got1 != want1 {
		t.Fatalf("got1: %q\nwant1: %q", got1, want1)
	}

	if got2 != want2 {
		t.Fatalf("got2: %q\nwant2: %q", got2, want2)
	}
}

func TestFilter_empty_name_panics(t *testing.T) {
	type F = ffilter.Filter

	type Fn func(string) F

	var filter F

	tests := []struct {
		name string
		fn   Fn
	}{
		{"New", filter.New},
		{"Chain", filter.Chain},
		{"In", filter.In},
		{"Out", filter.Out},
		{"Set", func(s string) F { return filter.Set(s, "val") }},
		{"SetInt", func(s string) F { return filter.SetInt(s, 1) }},
		{"SetFloat", func(s string) F { return filter.SetFloat(s, 1) }},
		{"SetU16", func(s string) F { return filter.SetU16(s, 1) }},
		{"SetSize", func(s string) F { return filter.SetSize(s, 1, 1) }},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				v := recover()
				if v == nil {
					t.Fatalf("Filter.%s allowed an empty name to be passed", tt.name)
				}
			}()

			tt.fn("")
		})
	}
}

func TestFilter_New_from_empty(t *testing.T) {
	f1 := ffilter.New("copy")
	f2 := ffilter.Filter{Prev: &f1}
	f3 := f2.New("acopy")
	got := f3.String()
	want := "copy;acopy"

	if got != want {
		t.Fatalf("got: %q, want: %q", got, want)
	}
}

func TestFilter_Chain_from_empty(t *testing.T) {
	f1 := ffilter.New("acopy").New("copy")
	f2 := ffilter.Filter{Prev: &f1, IsChain: true}
	f3 := f2.Chain("copy")
	got := f3.String()
	want := "acopy;copy,copy"

	if got != want {
		t.Fatalf("got: %q, want: %q", got, want)
	}
}

func TestFilter_Fuse(t *testing.T) {
	f0 := ffilter.Filter{}
	f1 := ffilter.New("a1").Chain("a2")
	f2 := ffilter.Filter{}
	f3 := ffilter.New("a3").Chain("a4")
	got := f0.Fuse(f1).Fuse(f2).Fuse(f3).String()
	want := "a1,a2,a3,a4"

	if got != want {
		t.Fatalf("got: %q, want: %q", got, want)
	}
}

func TestFilter_Join(t *testing.T) {
	f0 := ffilter.Filter{}
	f1 := ffilter.New("a1").Chain("a2")
	f2 := ffilter.Filter{}
	f3 := ffilter.New("b1").Chain("b2")
	got := f0.Join(f1).Join(f2).Join(f3).String()
	want := "a1,a2;b1,b2"

	if got != want {
		t.Fatalf("got: %q, want: %q", got, want)
	}
}
