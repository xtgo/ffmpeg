package ffilter_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/xtgo/ffmpeg/ffilter"
)

func ExampleToFFMPEG() {
	filter := ffilter.New("hstack").In("vid1").In("vid2").Out("vo")
	filter = ffilter.ToFFMPEG(filter)
	fmt.Println(filter)

	// Output:
	// [0:v][1:v]hstack
}

func TestToFFMPEG_noop(t *testing.T) {
	want := ffilter.New("hstack").In("x").In("y").Out("z")
	got := ffilter.ToFFMPEG(want)

	if !reflect.DeepEqual(got, want) {
		msg := "ToFFMPEG transformed did not no-op when expected:\ngot:  %s\nwant: %s"
		t.Fatalf(msg, got, want)
	}

	f1, f2 := &got, &want

	for f1 != nil && f2 != nil {
		if f1.Prev != f2.Prev {
			msg := "filters have different Prev pointers: %p != %p"
			t.Fatalf(msg, f1.Prev, f2.Prev)
		}

		f1, f2 = f1.Prev, f2.Prev
	}
}

func TestToFFMPEG_partial_replace(t *testing.T) {
	filter := ffilter.New("asplit").In("aid1").Out("ao").Out("awave").
		New("showwaves").In("awave").Out("vo")

	got := ffilter.ToFFMPEG(filter).String()
	want := "[0:a]asplit[awave];[awave]showwaves"

	if got != want {
		msg := "incorrect mpv -> ffmpeg dialect transform:\ngot:  %s\nwant: %s"
		t.Fatalf(msg, got, want)
	}
}
