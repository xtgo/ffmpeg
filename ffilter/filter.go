// Package ffilter provides ffmpeg filter construction.
package ffilter

import (
	"strconv"
)

// New initializes a filter graph with a filter of the given name.
// Methods can be called on the returned Filter
// to iteratively build the graph.
//
func New(name string) Filter {
	return Filter{}.New(name)
}

// Filter represents a single ffmpeg/libavfilter transformation,
// which can be linked to other Filters to form linear chains of filters,
// and graphs of linear chains.
//
// All methods will return a modified copy of the current filter,
// allowing for shared-prefix construction (both within and across filters),
// though concurrent construction of a diverging filter values is not safe.
//
type Filter struct {
	Name      string
	Prev      *Filter // reverse-linked-list.
	IsChain   bool    // true if part of a linear chain with Prev.
	Inputs    []string
	Args      []string
	NamedArgs []NamedArg
	Outputs   []string
}

// NamedArg represents a name=value pair
// that configures the behavior of a filter.
type NamedArg struct{ Name, Value string }

// String returns an ffmpeg/libavfilter filter description,
// suitable for passing as a command-line/library input.
func (f Filter) String() string {
	return string(f.AppendTo(nil))
}

// Format is like String, except that the returned string
// is spread over multiple lines spacing to ease reading.
func (f Filter) Format() string {
	return string(f.AppendFormattedTo(nil))
}

// New returns a new Filter that follows the receiver,
// representing a separate filter description.
// This is appropriate for constructing separate filter chains
// whose inputs and outputs do not have any simple, linear relationships.
//
// New will panic if name is empty.
//
func (f Filter) New(name string) Filter {
	if name == "" {
		panic("ffilter: Filter.New called with empty name")
	}

	return Filter{Name: name, Prev: findNonEmpty(&f)}
}

// Chain returns a new Filter that follows the receiver,
// representing an extra filter as part of the same linear chain.
// This is appropriate for filters whose outputs may implicitly
// form the inputs for the next filter in the chain.
//
// Chain will panic if name is empty.
//
func (f Filter) Chain(name string) Filter {
	if name == "" {
		panic("ffilter: Filter.Chain called with empty name")
	}

	fp := &f
	isChain := true

	if f.IsEmpty() {
		fp = fp.Prev
		isChain = f.IsChain
	}

	return Filter{Prev: fp, IsChain: isChain, Name: name}
}

// Join returns a copy of g linked to the end of f,
// as if New had been called on f to start constructing the head of g.
//
// Both f and g may each contain multiple distinct linear chains,
// and the distinction between Join and Fuse determines merely how
// the the last linear chain in f will be attached
// to the first linear chain in g.
//
func (f Filter) Join(g Filter) Filter {
	return concat(f, g, false)
}

// Fuse returns a copy of g linked to the end of f,
// as if Chain had been called on f to start constructing the head of g.
//
// Both f and g may each contain multiple distinct linear chains,
// and the distinction between Join and Fuse determines merely how
// the the last linear chain in f will be attached
// to the first linear chain in g.
//
func (f Filter) Fuse(g Filter) Filter {
	return concat(f, g, true)
}

// IsEmpty indicates that this filter contains no information
// (but it may link to another, possibly empty filter).
func (f Filter) IsEmpty() bool {
	// the name is the only required part of a filter
	return f.Name == ""
}

// In adds an input label to the current filter.
//
// In will panic if name is empty.
//
func (f Filter) In(name string) Filter {
	if name == "" {
		panic("ffilter: Filter.In called with empty name")
	}

	f.Inputs = safeStringAppend(f.Inputs, name)

	return f
}

// Out adds an output label to the current filter.
//
// In will panic if name is empty.
//
func (f Filter) Out(name string) Filter {
	if name == "" {
		panic("ffilter: Filter.Out called with empty name")
	}

	f.Outputs = safeStringAppend(f.Outputs, name)

	return f
}

// Set records a named argument in the current filter.
//
// Set will panic if name is empty.
//
func (f Filter) Set(name, val string) Filter {
	if name == "" {
		panic("ffilter: Filter.Set called with empty name")
	}

	arg := NamedArg{Name: name, Value: val}
	f.NamedArgs = safeNamedArgAppend(f.NamedArgs, arg)

	return f
}

// SetInt records an integer-valued, named argument in the current filter.
//
// SetInt will panic if name is empty.
//
func (f Filter) SetInt(name string, val int) Filter {
	if name == "" {
		panic("ffilter: Filter.SetInt called with empty name")
	}

	return f.Set(name, strconv.Itoa(val))
}

// SetFloat records an floating-point, named argument in the current filter.
//
// SetFloat will panic if name is empty.
//
func (f Filter) SetFloat(name string, val float64) Filter {
	if name == "" {
		panic("ffilter: Filter.SetFloat called with empty name")
	}

	return f.Set(name, strconv.FormatFloat(val, 'f', -1, 64))
}

// SetU16 records an uint16-valued, named argument in the current filter.
//
// SetU16 will panic if name is empty.
//
func (f Filter) SetU16(name string, val uint16) Filter {
	if name == "" {
		panic("ffilter: Filter.SetU16 called with empty name")
	}

	return f.SetInt(name, int(val))
}

// SetSize records an WxH named argument in the current filter;
// This shouldn't be used when width and height are separate filter arguments
// (such as when separated by a ':').
//
// SetSize will panic if name is empty.
//
func (f Filter) SetSize(name string, w, h uint16) Filter {
	if name == "" {
		panic("ffilter: Filter.SetSize called with empty name")
	}

	return f.Set(name, string(formatSize(nil, w, h)))
}

// Arg appends an unnamed argument to the current filter.
func (f Filter) Arg(val string) Filter {
	f.Args = safeStringAppend(f.Args, val)
	return f
}

// ArgInt appends an unnamed integer argument to the current filter.
func (f Filter) ArgInt(val int) Filter {
	return f.Arg(strconv.Itoa(val))
}

// ArgFloat appends an unnamed floating-point argument to the current filter.
func (f Filter) ArgFloat(val float64) Filter {
	return f.Arg(strconv.FormatFloat(val, 'f', -1, 64))
}

// ArgU16 appends an unnamed integer argument to the current filter.
func (f Filter) ArgU16(val uint16) Filter {
	return f.ArgInt(int(val))
}

// ArgSize appends a WxH argument to the current filter.
//
// This shouldn't be used when width and height are separate filter arguments
// (such as when separated by a ':').
//
func (f Filter) ArgSize(w, h uint16) Filter {
	return f.Arg(string(formatSize(nil, w, h)))
}

// AppendTo is like String,
// except that it appends the filter description to dst.
func (f Filter) AppendTo(dst []byte) []byte {
	sep := getSep(false)

	return format(dst, &f, &sep)
}

// AppendFormattedTo is like AppendTo,
// except that the appended contents are formatted across multiple lines.
func (f Filter) AppendFormattedTo(dst []byte) []byte {
	sep := getSep(true)

	return format(dst, &f, &sep)
}

// concat implements behavior common to Fuse and Join.
func concat(f, g Filter, isChain bool) Filter {
	if g.IsEmpty() {
		return f
	}

	if f.IsEmpty() {
		return g
	}

	if g.Prev == nil {
		// we've recursed to the head of g,
		// so mark whether the concatenation is part of a chain.
		g.IsChain = isChain
	} else {
		// recursively re-link g onto the tail of f,
		// making copies of each Filter node
		// in order to avoid side-effect mutation.
		f = concat(f, *g.Prev, isChain)
	}

	g.Prev = &f

	return g
}

// findNonEmpty returns the first Filter connected to (and including f)
// for which IsEmpty returns false.
func findNonEmpty(f *Filter) *Filter {
	for f != nil && f.IsEmpty() {
		f = f.Prev
	}

	return f
}

// safeStringAppend only uses the capacity of dst if the capacity is zero;
// otherwise, it appends to a newly allocated slice.
func safeStringAppend(dst []string, vals ...string) []string {
	i, j := len(dst), cap(dst)

	for _, v := range dst[i:j] {
		if v != "" {
			dst = dst[:i:i]
		}
	}

	return append(dst, vals...)
}

// safeNamedArgAppend only uses the capacity of dst if the capacity is zero;
// otherwise, it appends to a newly allocated slice.
func safeNamedArgAppend(dst []NamedArg, vals ...NamedArg) []NamedArg {
	i, j := len(dst), cap(dst)

	for _, v := range dst[i:j] {
		if v != (NamedArg{}) {
			dst = dst[:i:i]
		}
	}

	return append(dst, vals...)
}
